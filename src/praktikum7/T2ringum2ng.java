package praktikum7;

import praktikum6.Meetodid;

public class T2ringum2ng {

	public static void main(String[] args) {
		
		int kasutajaRaha = 100;
		
		while (kasutajaRaha > 0) {		
			System.out.println("Sul on: " + kasutajaRaha + " raha");
			int maxPanus = Math.min(25, kasutajaRaha);
			int panus = Meetodid.kasutajaSisestus("Palun sisesta panus (maksimaalselt " + maxPanus + ")", 1, maxPanus);
			kasutajaRaha -= panus;
			
			int kasutajaT2ring = Meetodid.kasutajaSisestus("Millisele tärngu tulemusele panustad? (1-6)", 1, 6);
			int t2ringuVise = Meetodid.suvalineArv(1, 6);
			
			if (kasutajaT2ring == t2ringuVise) {
				System.out.println("Arvasid õigesti, saad raha kuuekordselt tagasi!");
				kasutajaRaha += panus * 6;
			} else {
				System.out.println("Panid mööda, tuli hoopis " + t2ringuVise);
			}
		}
		System.out.println("Raha otsas, mäng läbi!");
		
	}
	
}