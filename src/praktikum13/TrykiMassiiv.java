package praktikum13;

public class TrykiMassiiv {

	public static void main(String[] args) {

		int[] arvud = { 3, 5, 7, 2, 4, 10 };
		int[][] neo = { { 1, 1, 1, 1, 1 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };

		tryki(arvud);
		tryki(neo);
		tryki(ridadeSummad(neo));

	}

	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}
		System.out.println();
	}

	public static void tryki(int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				System.out.print(maatriks[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}

		return summad;
	}

	public static int reaSumma(int[] massiiv) {
		int summa = 0;
		for (int arv : massiiv) {
			summa += arv;

		}
		return summa;
	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i + j == maatriks.length - 1) {
					summa += maatriks[i][j];

				}
			}

		}

		return 0;
	}

}