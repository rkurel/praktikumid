package praktikum5;

public class Tsyklid {

	public static void main(String[] args) {
		
		if (true) {
			System.out.println("tingimus on tõene");
		}
		
		int arv = 0;
		while (arv < 3) {
			System.out.println("tingimus on tõene (while), arv: " + arv);
			arv = arv + 1;
			//break; // katkestab tsükli töö
			//continue; // läheb tsükli algusesse 
		}

		for (int i = 0; i < 10; i++ /* i = i + 1 */) {
			System.out.println(i);
		}
 
	}

}