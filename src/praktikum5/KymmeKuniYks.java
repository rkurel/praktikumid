package praktikum5;

public class KymmeKuniYks {

	public static void main(String[] args) {
		
		// i++ // i = i + 1
		// i-- // i = i - 1
		
		// i += 2 // i = i + 2
		// i -= 2 // i = i - 2
		
		for (int i = 10; i > 0; i--) {
			System.out.print(i + " ");
		}
		
		System.out.println();

		for (int i = 0; i < 10; i++) {
			System.out.print(10 - i + " ");
		}
		
	}

}