package Pommitamine;

import java.util.Scanner;

/**
 * Lõpuprojekt. Ühepoolne laevadepommitamise sarnane mäng.
 * Arvuti küsib peale igat käiku, kuhu tahad lasta ja kuvab selle väljale.
 * Projekti alguseks võib lugeda 15 november.
 * Kogu töö jooksul tehtud väga palju muudatusi.
 * @author Rando Kurel
 */

public class Pommitamine {
	Scanner input = new Scanner(System.in);
	public static final boolean DEBUG = false;

	public static void main(String[] arg) {
		int v2ljasuurus = 8; // m22rab v2ljaku suuruse
		String[][] v2ljak = new String[v2ljasuurus][v2ljasuurus];
		M2nguv2ljak(v2ljak);
		int laevasuurus = 4; // määrab laeva suuruse
		Laev(v2ljak, laevasuurus);
		int torpeedod = 50; // laskude arv
		int lasud = 0;
		while (torpeedod > 0 && lasud < laevasuurus) {
			N2itav2lja(v2ljak);
			lasud = Tulistamine(v2ljak, lasud, torpeedod, v2ljasuurus);
			torpeedod--;
		}
		L6petamine(lasud, torpeedod, laevasuurus);
	}// main

	public static void Tyhirida() {
		System.out.println("_________________________________");
		System.out.println("");
	}// Tyhirida

	public static void M2nguv2ljak(String[][] v2ljak) { // m2nguv2ljaku baas
		for (int r = 0; r < v2ljak.length; r++) {
			for (int c = 0; c < v2ljak[0].length; c++) {
				v2ljak[r][c] = "~";
			}
		}
	}// m2nguv2ljak

	public static void N2itav2lja(String[][] v2ljak) {// joonistab m2nguv2ljaku
														// ekraanile
		Tyhirida();
		for (int r = 0; r < v2ljak.length; r++) {
			if (DEBUG == true) {
				for (int c = 0; c < v2ljak[0].length; c++) {
					System.out.print(" " + v2ljak[r][c]);
				}
				System.out.println("");
			} else {
				for (int c = 0; c < v2ljak[0].length; c++) {
					if (v2ljak[r][c].equals("S")) {
						System.out.print(" " + "~");
					} else {
						System.out.print(" " + v2ljak[r][c]);
					}
				}
				System.out.println("");
			}
		}
		Tyhirida();
	}// n2itav2lja

	public static void Laev(String[][] v2ljak, int laevasuurus) { // laeva asetsus
															// v2ljakul
		if (Math.random() < 0.5) {
			int col = (int) (Math.random() * 5);
			int row = (int) (Math.random() * 7);
			for (int i = 0; i < laevasuurus; i++) {
				v2ljak[row][col + i] = "S";
			}
		} else {
			int col = (int) (Math.random() * 7);
			int row = (int) (Math.random() * 5);
			for (int i = 0; i < laevasuurus; i++) {
				v2ljak[row + i][col] = "S";
			}
		}
	}// laev

	public static int Tulistamine(String[][] v2ljak, int lasud, int torpeedod, int v2ljasuurus) {
		Scanner input = new Scanner(System.in);
		int row, col;
		System.out.println("Sul on: " + torpeedod + " lasku alles!");
		System.out.println("Vali mitmes rida: ");
		row = input.nextInt();
		while (row > v2ljasuurus || row < 1) // kontrollib kas panid sobiva
												// arvu. Rida

		{
			System.out.println("Sisesta sobiv rida (1 -> " + v2ljasuurus + ")");
			row = input.nextInt();
		}
		System.out.println("Vali mitmes tulp: ");
		col = input.nextInt();
		while (col > v2ljasuurus || col < 1) // kontrollib kas panid sobiva
												// arvu. Tulp

		{
			System.out.println("Sisesta sobiv tulp (1 -> " + v2ljasuurus + " )");
			col = input.nextInt();
		}
		if (v2ljak[row - 1][col - 1].equals("S")) {
			lasud++;
			System.out.println("~~~~~~~ PIHTAS ~~~~~~~");
			v2ljak[row - 1][col - 1] = "X";
		} else {
			System.out.println("~~~~~~~ MÖÖDAS ~~~~~~~");
			v2ljak[row - 1][col - 1] = "O";
		}
		return lasud;
	}// tulistamine

	public static void L6petamine(int lasud, int torpeedod, int laevasuurus) {
		if (lasud < laevasuurus) // annab kaotuss6numi kui m2ngu l6puks suutsid
								// v2hem kui X osale pihta saada
			System.out.println("Kahjuks kaotasite. Te ei suutnud laeva põhja lasta.");
		if (torpeedod < 1)
			System.out.println("Sul on kõik torpeedod otsas");
		else if (lasud >= laevasuurus) { // annab v2idu s6numi kui X korda on
										// pihtas s6num
			// tulnud
			System.out.println("Sa lasid vastaste laeva põhja. Sa võitsid!");
		}
		System.out.println("Mäng sai läbi!");
	}// l6petamine

}// pommitamine
