package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class PudeliHari extends Applet {
    /*
     * Ringjoone vorrand parameetrilisel kujul
     * x = r * cos(t)
     * y = r * sin(t)
     * t = -PI..PI
     */
    public void paint(Graphics g) {
    	int w = getWidth(); //akna suurus
    	int h = getHeight();
    	
    	int x0 = w / 2; // Keskpunkt
        int y0 = h / 2;
        int r = Math.min(x0, y0); // Raadius; alati ekraani suurus, v6ib ka fikseeritud suurus olla
        int x, y;
        double t;


        // Taidame tausta
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        // Joonistame
        g.setColor(Color.black);

        for (t = -Math.PI; t < Math.PI; t = t + Math.PI / 16) {
            x = (int) (r * Math.cos(t) + x0);
            y = (int) (r * Math.sin(t) + y0);
            g.drawLine(x0, y0, x, y);
        }
    }
}