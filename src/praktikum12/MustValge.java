package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class MustValge extends Applet {

	@Override
	public void paint(Graphics g){
		int w = getWidth();//kasti suurus
		int h = getHeight();
		
		for (int y = 0; y < h; y++){
			
			double konser = (double) y / h;
			int juice =  (int) (konser * 255);
			Color color = new Color(juice, juice ,juice);
			g.setColor(color);
			
			g.drawLine(0, y, w, y);
		}
		
	}

}