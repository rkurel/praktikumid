package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutud {

	public static void main(String[] args) {
		
		String[] meheNimed = {"Henrik", "Paul", "Carlos", "Kert"};
		String[] naiseNimed = {"Kertliina", "Carlosliina", "Pauliina", "Henlociina"};
		String[] tegusonad = {"teevad musta tööd", "laamendavad", "teevad lapse voodi korda", "vaatasid voodis nii hästi filmi, et voodi läks katki"};
		
		String mees = SuvalineElement(meheNimed);
		String naine = SuvalineElement(naiseNimed);
		String tegevus = SuvalineElement(tegusonad);
		
		System.out.format("%s ja %s %s", mees, naine, tegevus);
		
		
	}

	public static String SuvalineElement(String[] s6nad) {
		return s6nad[Meetodid.suvalineArv(0, s6nad.length - 1)];
	}
}