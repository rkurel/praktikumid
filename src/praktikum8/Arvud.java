package praktikum8;

import lib.TextIO;

public class Arvud {

	public static void main(String[] args) {
		
		int arvud[] = new int[10];
		
		for (int i = 0; i < arvud.length; i++) {
			System.out.println("Palun sisesta arv masiivi indeksile " + i);
			arvud[i] = TextIO.getlnInt();
		}
		
		int[] arvudTagurpidi = tagurpidi(arvud);
	
		for (int i = 0; i < arvudTagurpidi.length; i++) {
			System.out.println(arvudTagurpidi[i]);
		}
		
	}
	
	public static int[] tagurpidi(int[] numbrid) {
		int[] tagurpidi = new int[numbrid.length];
		for (int i = 0; i < tagurpidi.length; i++) {
			int indeks = numbrid.length - 1 - i;
			tagurpidi[i] = numbrid[indeks];
		}
		return tagurpidi;
	}
	
	
}